#!/usr/bin/env python
# coding: utf-8

# In[1]:


import matplotlib.pyplot as plt


# In[2]:


epochs = 8
params_CNN = [6945, 8600, 21840, 69540]
params_FC = [39760, 282160, 515960, 663760]#4154710]
test_accuracy_CNN_CEL = [95.76, 97.81, 98.14, 98.56]
test_accuracy_CNN_NLLL = [95.66, 97.93, 98.15, 98.58]
test_accuracy_FC_CEL = [93.48, 94.36, 94.99, 95.28]#95.83]
test_accuracy_FC_NLLL = [93.3, 94.58, 95.4, 95.51]#96.15]
valid_accuracy_CNN_CEL = [9.25, 83.05, 95.35, 97, 97.2, 97.65, 97.8, 97.95, 98.5]
valid_accuracy_CNN_NLLL = [11.7, 79, 94.15, 96.45, 96.65, 97.85, 98.25, 98.35, 98.6]
valid_accuracy_FC_CEL = [9.25, 73.9, 88.25, 90, 91.75, 93.3, 94.6, 95.2, 95.35]
valid_accuracy_FC_NLLL = [5.35, 60.3, 86.5, 89.45, 90.8, 92.3, 92.45, 94.15, 94.7]


# In[3]:


plt.figure(figsize=(15,10))
plt.plot(params_FC, test_accuracy_FC_CEL, '-o', color='blue', label='FC_CrossEntropyLoss')
plt.plot(params_FC, test_accuracy_FC_NLLL, '-X', color='orange', label='FC_NLLLoss')
plt.ylim(0,100)
plt.xlabel("Number of parameters in network", fontsize=18)
plt.ylabel("Test accuracy [%]", fontsize=18)
plt.title("Statistics for Fully connected models, tested on 8000 MNIST images", fontsize=18)
plt.legend(fontsize=20)
plt.show()


# In[4]:


plt.figure(figsize=(15,10))
plt.plot(params_FC, test_accuracy_FC_CEL, '-o', color='blue', label='FC_CrossEntropyLoss')
plt.plot(params_FC, test_accuracy_FC_NLLL, '-X', color='orange', label='FC_NLLLoss')
plt.xlabel("Number of parameters in network", fontsize=18)
plt.ylabel("Test accuracy [%]", fontsize=18)
plt.title("Statistics for Fully connected models, tested on 8000 MNIST images, zoomed in", fontsize=18)
plt.legend(fontsize=20)
plt.show()


# In[5]:


plt.figure(figsize=(15,10))
plt.plot(params_CNN, test_accuracy_CNN_CEL, '-o', color='red', label='CNN_CrossEntropyLoss')
plt.plot(params_CNN, test_accuracy_CNN_NLLL, '-o', color='green', label='CNN_NLLLoss')
plt.ylim(0,100)
plt.xlabel("Number of parameters in network", fontsize=18)
plt.ylabel("Test accuracy [%]", fontsize=18)
plt.title("Statistics for Convolutional models, tested on 8000 MNIST images", fontsize=18)
plt.legend(fontsize=20)
plt.show()


# In[6]:


plt.figure(figsize=(15,10))
plt.plot(params_CNN, test_accuracy_CNN_CEL, '-o', color='red', label='CNN_CrossEntropyLoss')
plt.plot(params_CNN, test_accuracy_CNN_NLLL, '-o', color='green', label='CNN_NLLLoss')
plt.xlabel("Number of parameters in network", fontsize=18)
plt.ylabel("Test accuracy [%]", fontsize=18)
plt.title("Statistics for Convolutional models, tested on 8000 MNIST images, zoomed in", fontsize=18)
plt.legend(fontsize=20)
plt.show()


# In[9]:


plt.figure(figsize=(15,10))
plt.plot(params_CNN, test_accuracy_CNN_CEL, '-o', color='red', label='CNN_1')
plt.plot(params_CNN, test_accuracy_CNN_NLLL, '-o', color='green', label='CNN_2')
plt.plot(params_FC, test_accuracy_FC_CEL, '-o', color='blue', label='FC_1')
plt.plot(params_FC, test_accuracy_FC_NLLL, '-o', color='orange', label='FC_2')
#plt.plot(11175370, 98.56, 'o', color='black', label='resnet18')
plt.xlabel("Number of parameters in network", fontsize=18)
plt.ylabel("Test accuracy [%]", fontsize=18)
plt.title("Statistics for all models, tested on 8000 MNIST images, zoomed in", fontsize=18)
plt.legend(fontsize=20)
plt.show()


# In[8]:


plt.figure(figsize=(15,10))
plt.plot(range(epochs + 1), valid_accuracy_CNN_CEL, '-o', color='red', label='CNN_CrossEntropyLoss')
plt.plot(range(epochs + 1), valid_accuracy_CNN_NLLL, '-o', color='green', label='CNN_NLLLoss')
plt.plot(range(epochs + 1), valid_accuracy_FC_CEL, '-o', color='blue', label='FC_CrossEntropyLoss')
plt.plot(range(epochs + 1), valid_accuracy_FC_NLLL, '-o', color='orange', label='FC_NLLLoss')
plt.xlabel("Epoch", fontsize=18)
plt.ylabel("Validation accuracy [%]", fontsize=18)
plt.title("Statistics for all models, validated on 2000 MNIST images", fontsize=18)
plt.legend(fontsize=20)
plt.show()


# In[ ]:




