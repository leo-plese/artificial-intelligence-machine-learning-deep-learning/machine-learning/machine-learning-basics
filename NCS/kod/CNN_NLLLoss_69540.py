#!/usr/bin/env python
# coding: utf-8

# In[1]:


import torch
import torchvision
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import matplotlib.pyplot as plt
import time
import onnx
import numpy as np
import math
import torch.nn.functional as nnf
import onnxruntime
from torchvision import transforms

epochs = 8
batch_size = 64
learning_rate = 0.001
momentum = 0.9
print_interval = 200
accuracy_list = list()
device = torch.device("cpu")


mnist_trainset = torchvision.datasets.MNIST(root='~/data', train=True, download=True, transform=transforms.Compose([transforms.Grayscale(num_output_channels=1), transforms.ToTensor(),
         transforms.Normalize(mean=[0.5], std=[0.5])]))
mnist_testset = torchvision.datasets.MNIST(root='~/data', train=False, download=True, transform=transforms.Compose([transforms.Grayscale(num_output_channels=1), transforms.ToTensor(),
         transforms.Normalize(mean=[0.5], std=[0.5])]))

mnist_valset, mnist_testset = torch.utils.data.random_split(mnist_testset, [int(0.2 * len(mnist_testset)), int(0.8 * len(mnist_testset))])

train_loader = torch.utils.data.DataLoader(mnist_trainset, batch_size=batch_size, shuffle=True)
valid_loader = torch.utils.data.DataLoader(mnist_valset, batch_size=batch_size, shuffle=False)
test_loader = torch.utils.data.DataLoader(mnist_testset, batch_size=batch_size, shuffle=False)


# In[20]:


class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(1, 10, kernel_size=5)
        self.conv2 = nn.Conv2d(10, 20, kernel_size=5)
        self.fc1 = nn.Linear(320, 150)
        self.fc2 = nn.Linear(150, 100)
        self.fc3 = nn.Linear(100, 10)
        
    def forward(self, x):
        x = F.relu(F.max_pool2d(self.conv1(x), 2))
        x = F.relu(F.max_pool2d(self.conv2(x), 2))
        x = x.view(-1, 320)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return F.log_softmax(x, dim=1)


# In[21]:


network = Net()
criterion = nn.NLLLoss()
optimizer = optim.SGD(network.parameters(), lr=learning_rate,
                      momentum=momentum)
network.to(device)
pytorch_total_params = sum(p.numel() for p in network.parameters())


# In[22]:


def train(epoch):
    network.train()
    for batch_idx, (data, target) in enumerate(train_loader):
        data, target = data.to(device), target.to(device)
        optimizer.zero_grad()
        output = network(data)
        loss = F.nll_loss(output, target)
        loss.backward()
        optimizer.step()
        if batch_idx % print_interval == 0: #ispisujemo samo neke da ne bude nakracan ispis
              print('Train| Epoch {}| {}/{} ({:.0f}%)| Loss: {:.6f}'.format(
                    epoch, batch_idx * len(data), len(train_loader.dataset),
                    100. * batch_idx / len(train_loader), loss.item()))


# In[23]:


def valid(epoch):
    network.eval()
    network.to(device)
    test_loss = 0
    correct = 0
    with torch.no_grad():
        for data, target in valid_loader:
            data, target = data.to(device), target.to(device)
            output = network(data)
            test_loss += F.nll_loss(output, target, size_average=False).item()
            pred = output.data.max(1, keepdim=True)[1]
            correct += pred.eq(target.data.view_as(pred)).sum()
            t_k = time.time()
    test_loss /= len(valid_loader.dataset)
    print('Valid| Epoch {}| Avg. loss: {:.4f}| Accuracy: {}/{} ({:.2f}%)'.format(epoch, 
    test_loss, correct, len(valid_loader.dataset),
    100.*correct / len(valid_loader.dataset)))
    accuracy_list.append(100.*correct / len(valid_loader.dataset))


# In[24]:


def test():
    network.eval()
    network.to(device)
    test_loss = 0
    correct = 0
    with torch.no_grad():
        for data, target in test_loader:
            data, target = data.to(device), target.to(device)
            output = network(data)
            test_loss += F.nll_loss(output, target, size_average=False).item()
            pred = output.data.max(1, keepdim=True)[1]
            correct += pred.eq(target.data.view_as(pred)).sum()
            t_k = time.time()
    test_loss /= len(test_loader.dataset)
    print('Tests| Avg. loss: {:.4f}| Accuracy: {}/{} ({:.2f}%)'.format(test_loss, correct,
    len(test_loader.dataset),100.*correct / len(test_loader.dataset)))


# In[25]:


print("Training and validating network with {} parameters".format(pytorch_total_params))
valid(0)
print(" ")
for epoch in range(1, epochs + 1):
    time0 = time.time()
    train(epoch)
    valid(epoch)
    print("Running time for Epoch {}: {}\n".format(epoch, time.time() - time0))
print('Testing network with {} parameters'.format(pytorch_total_params))
test()


# In[ ]:


plt.figure(figsize =(15,10))
plt.plot(range(epochs + 1), accuracy_list, '-X')
plt.xlabel("Number of epochs")
plt.ylabel("Accuracy rate for validation")
plt.ylim(0, 100)
plt.title("CNN model with logSoftmax and NLLLoss for {} parameters".format(pytorch_total_params))
plt.show()


# In[ ]:


# set the model to inference mode
network.eval()


# In[ ]:




