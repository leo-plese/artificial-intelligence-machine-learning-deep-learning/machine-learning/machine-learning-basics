#!/usr/bin/env python
# coding: utf-8

# In[1]:


import torch
import torchvision
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import matplotlib.pyplot as plt
import time
import onnx
import numpy as np
import math
from torchvision.models.resnet import ResNet, BasicBlock
import onnxruntime
from torchvision import transforms
import torchvision.models as models


# In[2]:


network = models.resnet18(pretrained=True)


# In[3]:


n = 1000
q = 28       # 28, 128, 256, 512, 1024
batch_size = 1

device = torch.device('cpu')
network.eval()
network.to(device)
data = torch.randn(batch_size, 3, q, q)

with torch.no_grad():
    dataa = torch.randn(batch_size, 3, q, q)
    logits = network.forward(dataa)
    torch.cuda.synchronize()
    time0 = 1000*time.perf_counter()
    for _ in range (n):
        dataa = torch.randn(batch_size, 3, q, q)
        logits = network.forward(dataa)
        _, pred = logits.max(1)
        out = pred.data.byte().cpu()
        torch.cuda.synchronize()
    time1 = 1000*time.perf_counter()
fps = (1000*n)/(time1-time0)

print(fps)


# In[4]:


network.eval()
# Input to the model
x = torch.randn(1, 3, q, q,requires_grad=False)

# Export the model
torch.onnx.export(network,               # model being run
                  x,                         # model input (or a tuple for multiple inputs)
                  "resnet18_{}.onnx".format(q), # where to save the model (can be a file or file-like object)
                  export_params=True,        # store the trained parameter weights inside the model file
                  opset_version=10,          # the ONNX version to export the model to
                  do_constant_folding=True,  # whether to execute constant folding for optimization
                  input_names = ['input'],   # the model's input names
                  output_names = ['output'], # the model's output names
                  )


# In[5]:


ort_session = onnxruntime.InferenceSession("resnet18_{}.onnx".format(q))
torch_out = network(x)

def to_numpy(tensor):
    return tensor.detach().cpu().numpy() if tensor.requires_grad else tensor.cpu().numpy()

# compute ONNX Runtime output prediction
ort_inputs = {ort_session.get_inputs()[0].name: to_numpy(x)}
ort_outs = ort_session.run(None, ort_inputs)

# compare ONNX Runtime and PyTorch results
np.testing.assert_allclose(to_numpy(torch_out), ort_outs[0], rtol=1e-03, atol=1e-05)


# In[6]:


batch_size = 1
q = 512 # 128, 256, 512, 1024
device = torch.device('cuda')
network.eval()
network.to(device)

data = torch.randn(batch_size,3,q,q)

with torch.no_grad():
    data_network = torch.randn(batch_size,3,q,q).cuda()
    logits = network.forward(data_network)
    torch.cuda.synchronize()
    time0 = 1000*time.perf_counter()
    for _ in range (n):
        data_network = torch.randn(batch_size,3,q,q).cuda()
        logits = network.forward(data_network)
        _, pred = logits.max(1)
        out = pred.data.byte().cpu()
        torch.cuda.synchronize()
    time1 = 1000*time.perf_counter()
fps = (1000*n)/(time1-time0)
print(fps)


# In[ ]:




