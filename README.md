# Machine Learning Basics. MNIST Handwritten Digits Classification Using Deep Learning

Machine Learning Basics - implementation of: loss functions (cross entropy loss, L1 loss, L2 loss, hinge loss), regularizations (L1 regularizer, L2 regularizer, early stopping), gradient check, optimizers, training a simple deep model. Implementation in Python - NumPy and PyTorch.

Team project: 10 members organized in 5 groups with 2 members.

My contribution: part of code for loss functions and regularizers, together with corresponding parts of technical documentation.

Project duration: Sep 2019 - Feb 2020.
